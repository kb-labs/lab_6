#!/bin/bash

# run postgres entrypoint script with blank argument
/docker-entrypoint.sh "-"

gosu postgres postgres &
redis-server &
python3 /app/user/manage.py migrate
python3 /app/user/manage.py runserver 0.0.0.0:8000
