from django.db import models


class PrivateMessage(models.Model):
    owner = models.ForeignKey(to='chat.ChatUser')
    recipient = models.ForeignKey(to='chat.ChatUser', related_name='rec')
    message = models.TextField(verbose_name="message")
    created = models.DateTimeField(verbose_name="created", auto_now_add=True)

    @property
    def author(self):
        return "{} {}".format(self.owner.first_name, self.owner.last_name)

    class Meta:
        ordering = ['created']
        verbose_name = "Private messages"
        verbose_name_plural = "Private messages"
