from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import PrivateMessageTemplateView, PrivateMessageCreateView


urlpatterns = [
    url(r'^$', login_required(PrivateMessageTemplateView.as_view()),
        name='index'),
    url(r'^(?P<recipient>\d+)/$',
        login_required(PrivateMessageCreateView.as_view()),
        name='private')
]
