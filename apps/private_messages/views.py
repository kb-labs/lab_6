from django.db.models import Q
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, TemplateView

from apps.chat.models import ChatUser
from apps.main.views.mixins import AdminPageViewMixin
from .models import PrivateMessage


class PrivateMessageTemplateView(AdminPageViewMixin, TemplateView):
    template_name = 'private_messages/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_list = ChatUser.objects.all().exclude(is_superuser=True)

        context['user_list'] = [
            {
                "user": user,
                "messages_count": PrivateMessage.objects.filter(
                    Q(owner=self.request.user) | Q(recipient=user)).count()
            }
            for user in user_list
        ]
        return context


class PrivateMessageCreateView(AdminPageViewMixin, CreateView):
    model = PrivateMessage
    fields = ('message',)
    template_name = 'private_messages/private.html'

    def get_success_url(self):
        return reverse("private_messages:private",
                       kwargs={"recipient": self.recipient.pk})

    def dispatch(self, request, *args, **kwargs):
        recipient_pk = kwargs.get('recipient', 0)
        self.recipient = get_object_or_404(ChatUser, pk=recipient_pk)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner = self.request.user
        instance.recipient = self.recipient
        instance.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recipient'] = self.recipient
        context['message_list'] = PrivateMessage.objects.filter(
            (Q(owner=self.request.user) and Q(recipient=self.recipient)) |
            (Q(owner=self.recipient) and Q(recipient=self.request.user))
        )
        return context

