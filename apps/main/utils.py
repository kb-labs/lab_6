from django.conf import settings
from django import forms


def get_admin_media():
    extra = '' if settings.DEBUG else '.min'
    js = [
        'core.js',
        'vendor/jquery/jquery%s.js' % extra,
        'jquery.init.js',
        'admin/RelatedObjectLookups.js',
        'actions%s.js' % extra,
        'urlify.js',
        'prepopulate%s.js' % extra,
        'vendor/xregexp/xregexp%s.js' % extra,
    ]
    css = [
        'widgets.css',
    ]
    return forms.Media(
        js=['admin/js/%s' % url for url in js],
        css={'all': ['admin/css/%s' % url for url in css]}
    )
