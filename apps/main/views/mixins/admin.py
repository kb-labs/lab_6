from django.conf import settings

from ...utils import get_admin_media


class AdminPageViewMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['admin_media'] = get_admin_media()
        context['title'] = ""
        return context
