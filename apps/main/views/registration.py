from django import forms
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import CreateView

from apps.chat.models import ChatUser
from .mixins import AdminPageViewMixin


class UsersForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

    class Meta:
        model = ChatUser
        fields = ('email', 'first_name', 'last_name', 'password')
        labels = {
            "email": "Email"
        }


class RegistrationView(AdminPageViewMixin, CreateView):
    model = ChatUser
    form_class = UsersForm
    template_name = 'registration/registration.html'

    def get_success_url(self):
        return reverse('chat:index')

    def form_valid(self, form):
        instance = form.save(commit=False)
        email, password = instance.email, instance.password
        instance.set_password(instance.password)
        instance.save()

        user = authenticate(email=email, password=password)
        login(self.request, user)

        return HttpResponseRedirect(self.get_success_url())
