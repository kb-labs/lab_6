class CreatedMixin(object):
    def show_time(self, obj):
        return obj.created.time()

    show_time.short_description = 'Время создания'
    show_time.allow_tags = True

    def show_date(self, obj):
        return obj.created.date()

    show_date.short_description = 'Дата создания'
    show_date.allow_tags = True
