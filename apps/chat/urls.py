from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import ChatIndexPage


urlpatterns = [
    url(r'^$', login_required(ChatIndexPage.as_view()), name='index')
]
