import json
from channels import Group
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.db import models


class ChatUserManager(BaseUserManager):
    def create_user(self, email=None, password=None, user_role=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            user_role=user_role,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email=email,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class ChatUser(PermissionsMixin, AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', unique=True)
    first_name = models.CharField(verbose_name="Имя", max_length=255)
    last_name = models.CharField(verbose_name="Фамилия", max_length=255)

    is_active = models.BooleanField(
        verbose_name="Активность",
        default=True,
        null=False
    )
    is_staff = models.BooleanField(default=False, null=False)

    objects = ChatUserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class ChatMessage(models.Model):
    message = models.TextField(verbose_name="message")
    created = models.DateTimeField(verbose_name="created", auto_now_add=True)
    user = models.ForeignKey(ChatUser)

    @property
    def author(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)

    def send_to_chat(self, chat_name):
        Group(chat_name).send({
            "text": json.dumps({
                "created": self.created.strftime("%H:%M "),
                "message": self.message,
                "username": self.author,
            })
        })

    class Meta:
        ordering = ['created']
        verbose_name = "Chat message"
        verbose_name_plural = "Chat messages"
