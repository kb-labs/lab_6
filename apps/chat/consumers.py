from channels import Group
from channels.auth import channel_session_user_from_http, channel_session_user

from .models import ChatMessage


CHAT_NAME = "students-chat"


@channel_session_user_from_http
def ws_connect(message):
    Group(CHAT_NAME).add(message.reply_channel)


@channel_session_user
def ws_receive(message):
    message = ChatMessage.objects.create(
        user=message.user,
        message=message.content['text'],
    )
    message.send_to_chat(CHAT_NAME)


@channel_session_user
def ws_disconnect(message):
    Group(CHAT_NAME).discard(message.reply_channel)
