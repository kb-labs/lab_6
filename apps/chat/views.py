from django.views.generic import ListView

from apps.main.views.mixins import AdminPageViewMixin
from .models import ChatMessage


class ChatIndexPage(AdminPageViewMixin, ListView):
    template_name = 'chat/index.html'
    model = ChatMessage
