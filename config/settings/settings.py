import dj_database_url
import logging
import os

SECRET_KEY = "6)sfpinqw$qa$q9w=-=k7j29pcwgf$8tu@d(-jlcpa^6agb&$*"

ANONYMOUS_USER_ID = -1
SITE_ID = 1
ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

##################################################################
# Debug settings
##################################################################

# Set debug
DEBUG = os.environ.get('DEBUG', 'True') == 'True'

##################################################################
# Databases settings (for docker)
##################################################################

DATABASES = {
    'default': dj_database_url.config(),
}

##################################################################
# Logging settings
##################################################################

LOG_DATE_FORMAT = '%d %b %Y %H:%M:%S'

LOG_FORMATTER = logging.Formatter(
    u'%(asctime)s | %(levelname)-7s | %(name)s | %(message)s',
    datefmt=LOG_DATE_FORMAT)

CONSOLE_HANDLER = logging.StreamHandler()

CONSOLE_HANDLER.setFormatter(LOG_FORMATTER)

CONSOLE_HANDLER.setLevel(logging.DEBUG)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

##################################################################
# Assets settings
##################################################################

from os.path import dirname, basename, join

SETTINGS_PATH = dirname(__file__)
PROJECT_PATH = dirname(SETTINGS_PATH)
PROJECT_NAME = basename(PROJECT_PATH)
SERVER_PATH = dirname(PROJECT_PATH)
ROOT_PATH = dirname(SERVER_PATH)

FILE_UPLOAD_PERMISSIONS = 0o644

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = STATIC_URL
MEDIA_ROOT = join(SERVER_PATH, 'media')
MEDIA_URL = '/assets/'
STATICFILES_DIRS = ('static',)

##################################################################
# Finders, loaders, middleware and context processors
##################################################################

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.debug',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
            ],
        },
    },
]

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

AUTH_USER_MODEL = 'chat.ChatUser'
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
SESSION_COOKIE_HTTPONLY = False

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',)