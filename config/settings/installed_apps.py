INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.admin',
    'channels'
)

LOCAL_APPS = (
    'apps.main',
    'apps.chat',
    'apps.private_messages',
)

INSTALLED_APPS += LOCAL_APPS

MIGRATION_PATH = 'config.migrations.'

MIGRATION_MODULES = {
    'main': MIGRATION_PATH + 'main',
    'chat': MIGRATION_PATH + 'chat',
    'private_messages': MIGRATION_PATH + 'private_messages',
}
