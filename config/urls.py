from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login, logout

from apps.main.views import RegistrationView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', login, name='login'),
    url(r'^registration/', RegistrationView.as_view(), name='registration'),
    url(r'^logout/', logout, name='logout'),
    url(r'^chat/', include('apps.chat.urls', namespace='chat')),
    url(r'^private/', include('apps.private_messages.urls',
                              namespace='private_messages')),
]

urlpatterns += static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
