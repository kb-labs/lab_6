from channels import include


channel_routing = [
    include("apps.chat.routing.websocket_routing", path=r"^/chat/stream"),
]
